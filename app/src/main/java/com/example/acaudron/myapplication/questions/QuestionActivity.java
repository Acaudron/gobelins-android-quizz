package com.example.acaudron.myapplication.questions;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.acaudron.myapplication.R;

public class QuestionActivity extends AppCompatActivity {

    Question question;
    TextView textViewQuestionTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //get question
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        question = (Question) extras.getSerializable("QUESTION");

        //Set question title to text view
        textViewQuestionTitle = (TextView) findViewById(R.id.question_title);
        textViewQuestionTitle.setText(question.text);

        Log.d("question activity", String.valueOf(question));
        Log.d("question text", String.valueOf(question.text));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



    }

}
