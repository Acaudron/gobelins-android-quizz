package com.example.acaudron.myapplication.questions;


import android.content.Context;
import android.content.Intent;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.acaudron.myapplication.R;

import java.util.ArrayList;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 *
 */
public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionViewHolder> {

    private ArrayList<Question> questions;

    public QuestionsAdapter(ArrayList<Question> questions) {
        this.questions = questions;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final Context ctx = parent.getContext();
        final LayoutInflater inflater = LayoutInflater.from(ctx);
        final View questionItemView = inflater.inflate(R.layout.question_item, parent, false);
        return new QuestionViewHolder(questionItemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        Log.d("position", String.valueOf(position));
        final Question question = questions.get(position);
        final Context ctx = holder.itemView.getContext();
        holder.questionText.setText(question.text);
        holder.questionText.setBackgroundColor(question.getColorIdByDifficulty(ctx));

        holder.questionText.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, QuestionActivity.class);
                intent.putExtra("QUESTION", question);
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }


    class QuestionViewHolder extends RecyclerView.ViewHolder {
        TextView questionText;
        QuestionViewHolder(View itemView) {
            super(itemView);
            this.questionText = (TextView) itemView.findViewById(R.id.question_text);
        }
    }

}
