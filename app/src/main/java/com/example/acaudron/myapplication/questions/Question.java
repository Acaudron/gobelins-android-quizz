package com.example.acaudron.myapplication.questions;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.example.acaudron.myapplication.R;

import java.io.Serializable;

/**
 * Created by acaudron on 07/03/2017.
 */

public class Question implements Serializable{
    String text;
    private String difficulty;
    private String category;

    /**
     *
     * @param text
     * @param difficulty
     * @param category
     */
    public Question(String text, String difficulty, String category) {
        this.text = text;
        this.difficulty = difficulty;
        this.category = category;
    }

    /**
     * Return color id from context
     * @param ctx Context
     * @return int
     */
    protected int getColorIdByDifficulty(Context ctx) {
        int color_id;
        switch (this.difficulty) {
            case "easy":
                color_id = ContextCompat.getColor(ctx, R.color.colorDifficulty_easy);
                break;
            case "medium":
                color_id = ContextCompat.getColor(ctx, R.color.colorDifficulty_medium);
                break;
            case "hard":
                color_id = ContextCompat.getColor(ctx, R.color.colorDifficulty_hard);
                break;
            default:
                color_id = ContextCompat.getColor(ctx, R.color.colorDifficulty_default);
                break;
        }
        return color_id;
    }
}
